// @flow

import * as React from 'react';
import { Alert } from '../src/widgets.js';
import { shallow } from 'enzyme';

describe('Alert tests', () => {
  test('No alerts initially', () => {
    const wrapper = shallow(<Alert />);

    expect(wrapper.matchesElement(<></>)).toEqual(true);
  });

  test('Show alert message', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('test');

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.matchesElement(
          <>
            <div>
              test<button>&times;</button>
            </div>
          </>
        )
      ).toEqual(true);

      done();
    });
  });

  test('Close alert message', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('test');

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.matchesElement(
          <>
            <div>
              test<button>&times;</button>
            </div>
          </>
        )
      ).toEqual(true);

      wrapper.find('button.close').simulate('click');

      expect(wrapper.matchesElement(<></>)).toEqual(true);

      done();
    });
  });

  test('open 3 alerts, and close number 2', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('1')
    Alert.danger('2')
    Alert.danger('3')

    setTimeout(() => {
      expect(
        wrapper.matchesElement(
          <>
          <div>
            1<button>×</button>
          </div>
          <div>
            2<button>×</button>
          </div>
          <div>
            3<button>×</button>
          </div>
          </>
          )
      ).toEqual(true);

      wrapper.find('button.close').at(1).simulate('click');

      expect(
        wrapper.matchesElement(
          <>
          <div>
            1<button>×</button>
          </div>
          <div>
            3<button>×</button>
          </div>
          </>
          )
        ).toEqual(true);
        
      done();
    })
  })
});
